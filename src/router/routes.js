const routes = [
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      { path: "", name: "index", component: () => import("pages/Index.vue") },
      {
        path: "login",
        name: "login",
        component: () => import("pages/Auth.vue")
      },
      {
        path: "register",
        name: "register",
        component: () => import("pages/Auth.vue")
      },
      {
        path: "changePassword",
        name: "changePassword",
        component: () => import("pages/Auth.vue")
      },
      { path: "home", name: "home", component: () => import("pages/Home.vue") },
      { path: "faq", name: "faq", component: () => import("pages/FAQ.vue") },
      {
        path: "QoftheDay",
        name: "QoftheDay",
        component: () => import("pages/Q0ftheDay.vue")
      },
      {
        path: "discover",
        name: "discover",
        component: () => import("pages/discover.vue")
      },
      {
        path: "notification",
        name: "notification",
        component: () => import("pages/notification.vue")
      },
      {
        path: "feed",
        name: "feed",
        component: () => import("pages/feed.vue"),
        children: [
          {
            path: "grid",
            name: "grid",
            component: () => import("pages/feed.vue")
          },
          {
            path: "list",
            name: "list",
            component: () => import("pages/feed.vue")
          }
        ]
      },
      {
        path: "my-favourite",
        name: "my-favourite",
        component: () => import("pages/MyFavourite.vue")
      },
      {
        path: "profile/:slug",
        name: "profile",
        component: () => import("pages/profile.vue")
      },
      {
        path: "company",
        name: "company",
        component: () => import("pages/Company.vue")
      },
      {
        path: "university",
        name: "university",
        component: () => import("pages/university.vue")
      },
      {
        path: "theater",
        name: "theater",
        component: () => import("pages/theater.vue")
      },
      {
        path: "production",
        name: "production",
        component: () => import("pages/production.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
