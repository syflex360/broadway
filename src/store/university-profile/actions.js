import axios from "axios";

export function university({ commit }) {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.Api + "/api/profile/university")
      .then(response => {
        if (response.data.status == "success") {
          commit("university", response.data.data);
          resolve(response);
        }
      })
      .catch(err => {
        // LocalStorage.set('bwc-token', '')
        reject(err);
      });
  });
}
