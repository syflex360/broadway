import { LocalStorage } from "quasar";

export function user(state, data) {
  state.user = data;
}

export function notifications(state, data) {
  console.log(data);
  
  state.notifications = data;
}

export function logout(state) {
  state.user = null;
  state.isLoggedIn = null;
  LocalStorage.set("bwc-token", "");
}
