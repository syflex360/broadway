import { LocalStorage } from "quasar";

export default {
  user: null,
  product: null,
  token: LocalStorage.getItem("bwc-token") || "",
  notifications: null,
  isLoggedIn: LocalStorage.getItem("bwc-token")
    ? LocalStorage.getItem("bwc-token")
    : null
};
