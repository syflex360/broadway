export const isLoggedIn = state => state.isLoggedIn || "";

export function user(state) {
  return state.user;
}
export function notifications(state) {
  return state.notifications;
}
