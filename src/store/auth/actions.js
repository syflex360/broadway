import axios from "axios";
import { LocalStorage } from "quasar";

export function user({ commit }) {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.Api + "/api/user")
      .then(response => {
        if (response.data.status == "success") {
          commit("user", response.data.user);
          resolve(response);
        }
      })
      .catch(err => {
        // LocalStorage.set('bwc-token', '')
        reject(err);
      });
  });
}

export function notifications({ commit }) {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.Api + "/api/notification")
      .then(response => {
        if (response.data.status == "success") {
          commit("notifications", response.data.data);
          resolve(response);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
}

export function logout({ commit }) {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.Api + "/api/logout")
      .then(response => {
        commit("logout");
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });
}
