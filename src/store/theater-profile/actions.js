import axios from "axios";

export function theater({ commit }) {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.Api + "/api/profile/theater")
      .then(response => {
        if (response.data.status == "success") {
          commit("theater", response.data.data);
          resolve(response);
        }
      })
      .catch(err => {
        // LocalStorage.set('bwc-token', '')
        reject(err);
      });
  });
}
