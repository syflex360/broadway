import axios from "axios";

export function production({ commit }) {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.Api + "/api/profile/production")
      .then(response => {
        if (response.data.status == "success") {
          commit("production", response.data.data);
          resolve(response);
        }
      })
      .catch(err => {
        // LocalStorage.set('bwc-token', '')
        reject(err);
      });
  });
}
