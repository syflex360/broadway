import axios from "axios";

export function company({ commit }) {
  return new Promise((resolve, reject) => {
    axios
      .get(process.env.Api + "/api/profile/company")
      .then(response => {
        if (response.data.status == "success") {
          commit("company", response.data.data);
          resolve(response);
        }
      })
      .catch(err => {
        // LocalStorage.set('bwc-token', '')
        reject(err);
      });
  });
}
