import Vue from "vue";
import Vuex from "vuex";

import auth from "./auth";
import post from "./post";
import company from "./company-profile";
import university from "./university-profile";
import production from "./production-profile";
import theater from "./theater-profile";

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      auth,
      post,
      company,
      university,
      production,
      theater
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  return Store;
}
